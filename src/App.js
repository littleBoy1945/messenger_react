import MessengerRouter from "./screens/router"
import { BrowserRouter } from "react-router-dom"


function App() {

  return (
    <BrowserRouter>
      <div className="App">
        <MessengerRouter />
      </div>
    </BrowserRouter >
  );
}

export default App;
