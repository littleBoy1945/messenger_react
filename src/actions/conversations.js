export function initConversation(conversations) {
  return {
    type: "INIT_CONVERSATION",
    payload: conversations
  }
}

export function changeCurrentConversation(conversation_id) {
  return {
    type: "CHANGE_CURRENT_CONVERSATION",
    payload: conversation_id
  }
}

export function updateConversation(new_conversations) {
  return {
    type: "UPDATE_CONVERSATION",
    payload: new_conversations
  }
}

export function addConversation(conversations) {
  return {
    type: "ADD_CONVERSATION",
    payload: conversations
  }
}