export function initState(user) {
  return {
    type: "INIT_STATE",
    payload: user
  }
}
