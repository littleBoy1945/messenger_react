import { ConversationInit } from "./conversationInit"

export const searchConversation = (params) => {

  const instance = ConversationInit()

  return instance.post("/api/conversation/search", params)
}

export const selectUser = (params) => {
  const instance = ConversationInit()

  return instance.post("/api/conversation/selectUser", params)
}