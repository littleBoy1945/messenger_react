import config from "../../config.json"
import axios from "axios"

export const ConversationInit = () => {
  const jwt = localStorage.getItem("token")

  const instance = axios.create({
    baseURL: config.API_URL ,
    timeout: 5000,
    headers: { "Content-Type": "application/json", "Accept": "application/json" }
  });
  instance.defaults.headers.common['Authorization'] = "Bearer" + jwt;

  return instance
}
