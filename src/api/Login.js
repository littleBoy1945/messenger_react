import axios from "axios"
import config from "../config.json"

export const loginUser = (params) => {

  axios.post(
    config.API_URL + "/api/users/login",
    params, { "Content-Type": "application/json", "Accept": "application/json" }
  ).then(res => {
    localStorage.setItem("token", res.data.jwt)
    window.location = "/"
  }).catch(error => {
    alert(error.response.data.error)
  })
  
}