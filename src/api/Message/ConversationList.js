import { MessageInit } from "./messageInit"

export const conversationListApi = () => {

  const instance = MessageInit()

  return instance.get("/api/conversation_list/get")

}

