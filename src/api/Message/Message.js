import { MessageInit } from "./messageInit"

export const sendMessage = (params) => {

  const instance = MessageInit()

  return instance.post("/api/message/send", params)
}

export const getMessages = (params) => {

  const instance = MessageInit()

  return instance.post("/api/message/get_messages", params)

}


export const uploadPhotoInConversation = (params) => {
  const instance = MessageInit()

  return instance.post("/api/message/upload_photo", params)
}


export const deleteMessageApi = (params) => {
  const instance = MessageInit()

  return instance.post("/api/message/delete_message", params)
}