import axios from "axios"
import config from "../config.json"


export const registerUser = (params) => {

  return axios.post(
    config.API_URL + "/api/users/create",
    params, { "Content-Type": "application/json", "Accept": "application/json" }
  )
}

export const accountActivate = (params) => {
  
  return axios.post(
    config.API_URL + "/account-activation",
    params, { "Content-Type": "application/json", "Accept": "application/json" }
  )
}