import { UserInit } from "./userInit"

export const fetchCurrentUserInfo = () => {

  const instance = UserInit()

  return instance.get("/api/users/get")

}

export const fetchUserInfoWithConversation = (params) => {

  const instance = UserInit()

  return instance.post("/api/users/get", params)
}

export const processRelationship = (params) => {
  const instance = UserInit()

  return instance.post("/api/users/processRelationship", params)
}