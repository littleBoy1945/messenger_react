import React, { useState } from 'react';
import './Compose.css';
import TextField from '@material-ui/core/TextField';
import { useSelector } from 'react-redux';
import { sendMessage } from "../../api/Message/Message"

export default function Compose(props) {

  const [message, setMessage] = useState("")

  const conversations_list = useSelector(state => state.conversations)

  const handleChange = (event) => {
    setMessage(event.target.value)
  }

  const onKeyUp = (event) => {
    if (event.charCode === 13) {
      let current_conversation = conversations_list.filter((element) => {
        return element.is_current === true
      })
      let params = {
        conversation: current_conversation[0].conversation_id,
        message: message
      }
      sendMessage(params).then(response => {
        setMessage("")
      }).catch(err => {
        if(err.response){
          alert(err.response.data.error)
        }
      })
    }
  }

  return (
    <div className="compose">

      <TextField className="compose-input" id="standard-basic" label="Nhập tin nhắn ... " value={message} onChange={handleChange} onKeyPress={onKeyUp} />

      {
        props.rightItems
      }
    </div>
  );
}