import React, { useEffect } from "react"
import Toolbar from "../Toolbar/index"
import "./ConversationList.css"
import ConversationListItem from "../ConversationListItem/index"
import ConversationSearch from "../ConversationSearch/index"
import { conversationListApi } from "../../api/Message/ConversationList"
import { useSelector, useDispatch } from 'react-redux';
import { initConversation, changeCurrentConversation } from "../../actions/conversations"
import { fetchUserInfoWithConversation } from "../../api/User/User"
import { initState } from "../../actions/currentUserSearchForm"
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

export default function ConversationList() {

  const dispatch = useDispatch();

  const getConversation = () => {
    conversationListApi().then(response => {
      let newConversation = response.data.conversation;
      dispatch(initConversation(newConversation))
    })
  }

  useEffect(() => {
    getConversation()
// eslint-disable-next-line
  }, [])

  const conversations_list = useSelector(state => state.conversations)

  const currentConversationEvent = (conversation) => {
    let id = conversation.conversation_id
    dispatch(changeCurrentConversation(id))
    let formData = new FormData();
    formData.append("conversation_id", id)
    fetchUserInfoWithConversation(formData).then(response => {
      let user = response.data.user
      user.relationship = response.data.relationship
      user.curr_user_accepted_relationship = response.data.curr_user_accepted_relationship
      dispatch(initState(user))
    }).catch( err => {
      console.log(err)
    })
  }

  const handleLogOut = () =>{
    localStorage.clear()
    window.location = "/"
  }

  return (
    <div className="conversation-list">
      <Toolbar
        title="Chat"
        leftItems={[
          <AccountCircleIcon style={{ color: "#007aff", cursor: "pointer" }} key="settings-icon-conn-lst" />
        ]}
        rightItems={[
          <ExitToAppIcon style={{ color: "#007aff", cursor: "pointer" }} key="add-circle-conn-lst" onClick = {handleLogOut} />
        ]}
      />
      <ConversationSearch />
      {
        conversations_list.length > 0 ?
          conversations_list.map(conversation =>
            <div onClick={() => currentConversationEvent(conversation)} key={conversation.conversation_id}>
              <ConversationListItem
                key={conversation.conversation_id}
                data={conversation}
              />
            </div>
          )
          :
          <></>
      }
    </div>
  )
}
