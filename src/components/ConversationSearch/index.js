import React, { useState, useEffect } from "react"
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import { searchConversation, selectUser } from "../../api/Conversation/conversation"
import { useDispatch } from 'react-redux';
import { addConversation } from "../../actions/conversations"
import {initState} from "../../actions/currentUserSearchForm"
import "./ConversationSearch.css"

export default function ConversationSearch() {
  const [conversations, setConversations] = useState([])
  const [keyWord, setKeyWord] = useState("")

  const dispatch = useDispatch();

  useEffect(() => {
    if (keyWord !== "") {
      let params = {
        key: keyWord,
      }
      searchConversation(params).then(response => {
        setConversations(response.data.users)
      }).catch(err => {
        console.log(err)
      })
    }
  }, [keyWord])

  const handleClick = (event, value) => {
    if (value) {
      let params = {
        user_id: value.user_id
      }

      selectUser(params).then(response => {
        let conversation = response.data.conversation
        let user = response.data.user_selected
        user.relationship = response.data.relationship
        user.curr_user_accepted_relationship = response.data.curr_user_accepted_relationship
        dispatch(initState(user))
        dispatch(addConversation(conversation))
      }).catch(err => {
        console.log(err)
      })
    }
  }


  return (
    <div className="conversation-search">
      <Autocomplete
        id="search-conversation"
        freeSolo
        value={keyWord}
        getOptionLabel={(option) => {
          if (typeof option === 'string') {
            return option;
          }
          if (option.inputValue) {
            return option.inputValue;
          }
          return option.first_name + " ".concat(option.last_name);
        }}
        onInputChange={(event, newValue) => {
          setKeyWord(newValue)
        }}
        options={conversations}
        onChange={handleClick}
        renderInput={(params) => {
          return <TextField {...params} label="search" margin="normal" variant="outlined" />
        }}
      />
    </div>
  )
}

