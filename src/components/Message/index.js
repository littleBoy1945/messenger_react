import React from 'react';
import moment from 'moment';
import './Message.css';
import config from "../../config.json"
import DeleteIcon from '@material-ui/icons/Delete';
import { deleteMessageApi } from "../../api/Message/Message"

export default function Message(props) {
  const {
    data,
    isMine,
    startsSequence,
    endsSequence,
    showTimestamp
  } = props;
  const image_url = data.type_of_messages === 1 ? "" : config.API_URL + "/api/resources/photo?id=" + data.message_id + "." + data.content

  const deleteMessage = (data) => {
    let formData = new FormData();
    formData.append("message_id", data.message_id)
    deleteMessageApi(formData).then(response => {
      console.log(response.data)
    }).catch(err => {
      alert(err.response.data.message)
    })
  }
  
  const friendlyTimestamp = moment(data.created_at).format('LLLL');
  return (
    <div className={[
      'message',
      `${isMine ? 'mine' : ''}`,
      `${startsSequence ? 'start' : ''}`,
      `${endsSequence ? 'end' : ''}`
    ].join(' ')}>
      {
        showTimestamp &&
        <div className="timestamp">
          {friendlyTimestamp}
        </div>
      }

      <div className="bubble-container">
        <div className="bubble" title={friendlyTimestamp}>
          {
            data.type_of_messages === 1 ?
            data.content
            :
            <img src={image_url} alt = "message" className = "message-img" />
          }
        </div>
        {
          isMine ?
          (
            <div onClick = {() => deleteMessage(data)} className = "message-delete-icon">
              <DeleteIcon />
            </div>
          ):
          <></>
        }
      </div>
    </div>
  );
}