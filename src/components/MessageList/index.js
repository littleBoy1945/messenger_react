import React, { useEffect, useState } from 'react';
import Compose from "../Compose/index";
import Toolbar from "../Toolbar/index";
import Message from "../Message/index";
import moment from 'moment';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import VideocamIcon from '@material-ui/icons/Videocam';
import CallIcon from '@material-ui/icons/Call';
// import PhotoIcon from '@material-ui/icons/Photo';
import { getMessages, uploadPhotoInConversation } from "../../api/Message/Message"
import { useSelector } from "react-redux"
import processMessage from "./processMessage"
import sortMessages from "./sortMessage"
import "./MessageList.css"
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import IconButton from '@material-ui/core/IconButton';

export default function MessageList() {

  const [messages, setMessages] = useState([])
  const [title, setTitle] = useState("")
  const conversations_list = useSelector(state => state.conversations)

  useEffect(() => {
    if (conversations_list.length > 0) {
      const current_conv = conversations_list.filter(conversation => conversation.is_current === true)[0]
      setTitle(current_conv.title)
      const params = {
        current_conversation: current_conv.conversation_id
      }
      getMessages(params).then(response => {
        let messages_response = response.data.messages
        let new_messages = processMessage(messages_response)
        setMessages(new_messages)
      })

      setInterval(function () {
        let current_user = conversations_list.filter(conversation => conversation.is_current === true)[0]
        if (current_user.conversation_id) {
          let params = new FormData();
          params.append("current_conversation", current_user.conversation_id)
          getMessages(params).then(response => {
            let messages_response = response.data.messages
            let new_messages = processMessage(messages_response)

            setMessages(new_messages)
          })
        }
      }, 10000);
    }
  }, [conversations_list])

  const current_user = useSelector(state => state.user)

  const MY_USER_ID = current_user.user_id;

  const renderMessages = () => {
    sortMessages(messages)
    let i = 0;
    let messageCount = messages.length;
    let tempMessages = [];

    while (i < messageCount) {
      let previous = messages[i - 1];
      let current = messages[i];
      let next = messages[i + 1];
      let isMine = current.user_id === MY_USER_ID;
      let currentMoment = moment(current.created_at);
      let prevBySameAuthor = false;
      let nextBySameAuthor = false;
      let startsSequence = true;
      let endsSequence = true;
      let showTimestamp = true;

      if (previous) {
        let previousMoment = moment(previous.created_at);
        let previousDuration = moment.duration(currentMoment.diff(previousMoment));
        prevBySameAuthor = previous.user_id === current.user_id;

        if (prevBySameAuthor && previousDuration.as('hours') < 1) {
          startsSequence = false;
        }

        if (previousDuration.as('hours') < 1) {
          showTimestamp = false;
        }
      }

      if (next) {
        let nextMoment = moment(next.created_at);
        let nextDuration = moment.duration(nextMoment.diff(currentMoment));
        nextBySameAuthor = next.user_id === current.user_id;

        if (nextBySameAuthor && nextDuration.as('hours') < 1) {
          endsSequence = false;
        }
      }

      tempMessages.push(
        <Message
          key={i}
          isMine={isMine}
          startsSequence={startsSequence}
          endsSequence={endsSequence}
          showTimestamp={showTimestamp}
          data={current}
        />
      );

      // Proceed to the next message.
      i += 1;
    }

    return tempMessages;
  }

  const fileSelectedHandle = (event) => {
    const current_conv = conversations_list.filter(conversation => conversation.is_current === true)[0]
    let formData = new FormData()
    formData.append("photo", event.target.files[0])
    formData.append("conversation_id", current_conv.conversation_id)

    uploadPhotoInConversation(formData).then(response => {
      console.log(response)
    }).catch(err => {
      alert("err")
    })
  }

  return (
    <div className="message-list">
      <Toolbar
        title={title}
        rightItems={[
          <InfoOutlinedIcon className="message-right-item" key="info" />,
          <VideocamIcon className="message-right-item" key="video" />,
          <CallIcon className="message-right-item" key="call" />
        ]}
      />

      <div className="message-list-container">{renderMessages()}</div>

      <Compose rightItems={[
        <div>
          <input accept="image/*" className="message-upload-input" id="icon-button-file" type="file" onChange={fileSelectedHandle} />
          <label htmlFor="icon-button-file">
            <IconButton color="primary" aria-label="upload picture" component="span" id="icon-button-message-lst">
              <PhotoCamera id="photo-camera-message-lst" />
            </IconButton>
          </label>
        </div>
      ]} />
    </div>
  );
}