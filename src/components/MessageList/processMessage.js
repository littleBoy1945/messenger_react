export default function processMessage(messages_arr) {
  var merged_messages = []
  messages_arr.forEach(function (item) {
    let message_item = item.messages
    let new_item = message_item.map(function (el) {
      el.user_id = item.user_id
      return el
    })
    merged_messages.push(...new_item)
  })

  return merged_messages
}