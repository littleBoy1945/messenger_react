export default function sortMessages(messages) {
  let messages_arr = messages.sort((a, b) => {
    return ((new Date(a.created_at).getTime() - new Date(b.created_at).getTime()) > 0 ? 1 : -1)
  })
  return messages_arr
}