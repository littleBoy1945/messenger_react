import React, { useEffect } from "react"
import "./Messenger.css"
import ConversationList from "../ConversationList/index"
import MessageList from "../MessageList/index"
import Relation from "../Relation/index"
import { fetchCurrentUserInfo } from "../../api/User/User"
import { useDispatch } from 'react-redux';
import authorizeSuccessAction from "../../actions/user"

export default function Messenger(props) {


  const dispatch = useDispatch()

  useEffect(() => {
    fetchCurrentUserInfo().then(response => {
      let data = response.data
      dispatch(authorizeSuccessAction(data))
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[])

  return (
    <div className="messenger">

      <div className="scrollable sidebar">
        <ConversationList />
      </div>

      <div className="scrollable content">
        <MessageList />
      </div>

      <div className="scrollable info">
        <Relation />
      </div>
    </div>
  )
}
