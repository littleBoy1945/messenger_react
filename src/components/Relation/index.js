import React from "react"
import { useSelector } from 'react-redux';
import RelationOption from "../RelationOption/index"

export default function Relation() {

  const currentContactUser = useSelector(state => state.currentUserSearchForm)

  return (
    <div>
      {
        currentContactUser.user_id ?
          (
            <ul>
              <li>Name:{currentContactUser.name ? currentContactUser.name : "None"}</li>
              <li>Birth:{currentContactUser.birth ? currentContactUser.birth : "None"}</li>
              <li>Sex:{currentContactUser.sex ? currentContactUser.sex : "None"}</li>
              <li>Phone Number:{currentContactUser.phoneNumber ? currentContactUser.phoneNumber : "None"}</li>
              <li>Other name:{currentContactUser.otherName ? currentContactUser.otherName : "None"}</li>
              <li>Email:{currentContactUser.email ? currentContactUser.email : "None"}</li>
              <li>Relationship:{currentContactUser.relationship ? currentContactUser.relationship : "None"}</li>
            </ul>
          ) : ""
      }
      <div>
        <RelationOption currentContactUser = {currentContactUser}/>
      </div>
    </div>
  )
}