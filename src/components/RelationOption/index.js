import React, { useState, useEffect } from "react"
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import BlockIcon from '@material-ui/icons/Block';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import BookmarksIcon from '@material-ui/icons/Bookmarks';
import AutorenewIcon from '@material-ui/icons/Autorenew';
import { processRelationship } from "../../api/User/User";

export default function RelationOption(props) {

  const [relationshipStatus, setRelationshipStatus] = useState(0)
  const [currUserAcceptedRelationship, setCurrUserAcceptedRelationship] = useState(null) //curr_user_status_in_relationship

  const options = Object.freeze({
    "ADD_FRIEND": 1,
    "BLOCK": 2,
    "UNFRIEND": 3,
    "ACCEPT_INVITE": 4,
    "UNBLOCK": 5
  })

  let currentContactUser = props.currentContactUser

  useEffect(() => {
    setRelationshipStatus(currentContactUser.relationship)
    setCurrUserAcceptedRelationship(currentContactUser.curr_user_accepted_relationship)
  }, [currentContactUser])

  const handleClick = (option) => {
    let formData = new FormData();
    formData.append("option", option)
    formData.append("user_id", currentContactUser.user_id)
    processRelationship(formData).then(response => {
      setRelationshipStatus(response.data.relationship_status)
      setCurrUserAcceptedRelationship(response.data.curr_user_accepted_relationship)
    }).catch(err => {
      console.log(err.response.message)
    })
  }

  if (relationshipStatus === 0) {
    return (
      <List
        component="nav"
        aria-labelledby="nested-list-subheader"
        subheader={
          <ListSubheader component="div" id="nested-list-subheader">
            User Info
          </ListSubheader>
        }
      >
        <ListItem button onClick={() => handleClick(options.ADD_FRIEND)}>
          <ListItemIcon>
            <AddCircleOutlineIcon />
          </ListItemIcon>
          <ListItemText primary="Add friend" />
        </ListItem>
        <ListItem button onClick={() => handleClick(options.BLOCK)}>
          <ListItemIcon>
            <BlockIcon />
          </ListItemIcon>
          <ListItemText primary="Block" />
        </ListItem>
      </List>
    )
  } else if (relationshipStatus === 1) {
    return (
      <List
        component="nav"
        aria-labelledby="nested-list-subheader"
        subheader={
          <ListSubheader component="div" id="nested-list-subheader">
            User Info
          </ListSubheader>
        }
      >
        <ListItem button onClick={() => handleClick(options.UNFRIEND)}>
          <ListItemIcon>
            <HighlightOffIcon />
          </ListItemIcon>
          <ListItemText primary="Unfriend" />
        </ListItem>
        <ListItem button onClick={() => handleClick(options.BLOCK)}>
          <ListItemIcon>
            <BlockIcon />
          </ListItemIcon>
          <ListItemText primary="Block" />
        </ListItem>
      </List>
    )
  } else if (relationshipStatus === 2) {
    return (
      <List
        component="nav"
        aria-labelledby="nested-list-subheader"
        subheader={
          <ListSubheader component="div" id="nested-list-subheader">
            User Info
          </ListSubheader>
        }
      >
        {
          currUserAcceptedRelationship === true ?
            (
              <>
                <ListItem>
                  <ListItemIcon>
                    <BookmarksIcon />
                  </ListItemIcon>
                  <ListItemText primary="Waiting for accept" />
                </ListItem>
              </>
            ) :
            (
              <>
                <ListItem button onClick={() => handleClick(options.ACCEPT_INVITE)}>
                  <ListItemIcon>
                    <BookmarksIcon />
                  </ListItemIcon>
                  <ListItemText primary="Accept friend" />
                </ListItem>
              </>
            )
        }
      </List>
    )
  } else if (relationshipStatus === 3) {
    return (
      <List
        component="nav"
        aria-labelledby="nested-list-subheader"
        subheader={
          <ListSubheader component="div" id="nested-list-subheader">
            User Info
          </ListSubheader>
        }
      >
        {
          currUserAcceptedRelationship === true ?
            (
              <>
                <ListItem button onClick={() => handleClick(options.UNBLOCK)}>
                  <ListItemIcon>
                    <AutorenewIcon />
                  </ListItemIcon>
                  <ListItemText primary="Unblock" />
                </ListItem>
              </>
            ):
            (
              <></>
            )
        }
      </List>
    )
  } else if (relationshipStatus === 4) {
    return (
      <List
        component="nav"
        aria-labelledby="nested-list-subheader"
        subheader={
          <ListSubheader component="div" id="nested-list-subheader">
            User Info
        </ListSubheader>
        }
      >
        <ListItem button onClick={() => handleClick(options.ADD_FRIEND)}>
          <ListItemIcon>
            <AddCircleOutlineIcon />
          </ListItemIcon>
          <ListItemText primary="Add friend" />
        </ListItem>
        <ListItem button onClick={() => handleClick(options.BLOCK)}>
          <ListItemIcon>
            <BlockIcon />
          </ListItemIcon>
          <ListItemText primary="Block" />
        </ListItem>
      </List>
    )
  } else {
    return (
      <></>
    )
  }
}