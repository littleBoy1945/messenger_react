import { useState, useEffect } from 'react' // eslint-disable-next-line 

const useForm = (callback, validate) => {

  const [values, setValues] = useState({})
  // const [redirect, setRedirect] = useState({})
  const [errors, setErrors] = useState({})
  const [isSubmitted, setIsSubmitted] = useState(false)

  const notEmpty = (value) => value !== ""

  useEffect(() => {
    if(Object.keys(errors).length === 0 && isSubmitted){
      if(Object.values(values).every(notEmpty)){
        callback(values)
      }
    }// eslint-disable-next-line
  },[isSubmitted]); 

  const handleSubmit = (event) =>{
    if (event) {
      event.preventDefault();
    }
    setErrors(validate(values))
    setIsSubmitted(true)
    return isSubmitted
  }

  const handleChange = (event) =>{
    event.persist()
    setValues( values => ({ ...values, [event.target.name]: event.target.value}));
  }

  return {
    values,
    errors,
    handleChange,
    handleSubmit
  }
}

export default useForm