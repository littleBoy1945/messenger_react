const initialState = []

export default function Conversation(state = initialState, action) {
  switch (action.type) {
    case "INIT_CONVERSATION":
      if (action.payload.length > 0) {
        var action_mapping = action.payload.map((item) => {
          item.is_current = false
          return item
        })
        action_mapping[0].is_current = true
        return state.concat(action_mapping)
      }
      return state

    case "CHANGE_CURRENT_CONVERSATION":
      if (state.length > 0) {
        return state.map((conv) => {
          if (conv.conversation_id === action.payload) {
            conv.is_current = true
          } else {
            conv.is_current = false
          }
          return conv
        })
      } else {
        return state
      }

    case "ADD_CONVERSATION":
      
      let list_conversation_id = state.map((item) => {
        return item.conversation_id
      })
      let current_conversation = action.payload
      if(list_conversation_id.includes(current_conversation.conversation_id)){
        return state.map((item) => {
          if(item.conversation_id === current_conversation.conversation_id){
            item.is_current = true
            return item
          }else{
            item.is_current = false
            return item
          }
        })
      }else{
        return [current_conversation].map((item) => {
          item.is_current = true
          return item
        }).concat(state.map((item) => {
          item.is_current = false
          return item
        }))
      }


    default:
      return state

  }
}