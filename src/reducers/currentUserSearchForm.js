const initialState = {
  user_id: "",
  name: "",
  birth: "",
  sex: "",
  phoneNumber: "",
  otherName: "",
  email: "",
  relationship: "",
  curr_user_accepted_relationship: ""
}

export default function CurrentUserSearchForm(state = initialState, action) {
  switch (action.type) {
    case "INIT_STATE":
      const payload = action.payload
      state.user_id = payload.user_id
      state.name = payload.first_name + " ".concat(payload.last_name)
      state.birth = payload.birth
      state.sex = payload.sex
      state.phoneNumber = payload.phoneNumber
      state.otherName = payload.otherName
      state.email = payload.email
      state.relationship = payload.relationship
      state.curr_user_accepted_relationship = payload.curr_user_accepted_relationship
      return {...state}    
      
    default:
      return state
  }
}
