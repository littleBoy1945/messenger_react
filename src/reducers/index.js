import { combineReducers} from "redux";
import user from "./user";
import Conversation from "./conversations"
import CurrentUserSearchForm from "./currentUserSearchForm"

const rootReducer = combineReducers({
  user: user,
  conversations: Conversation,
  currentUserSearchForm: CurrentUserSearchForm
})

export default rootReducer