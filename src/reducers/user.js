const initialState = {
  first_name: "",
  last_name: "",
  user_id: "",
};

const user = (state = initialState, action) => {

  // return state;
  switch (action.type) {
    case "USER_AUTHORIZE_SUCCESS":
      state.first_name = action.payload.first_name;
      state.last_name = action.payload.last_name;
      state.user_id = action.payload.user_id
      return state

    default:
      return state
  }
};

export default user;