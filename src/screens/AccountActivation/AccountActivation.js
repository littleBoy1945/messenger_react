import React, { useEffect, useState } from "react"
import "./AccountActivation.css"
import CircularProgress from '@material-ui/core/CircularProgress';
import { accountActivate } from "../../api/Register"
import { fetchCurrentUserInfo} from "../../api/User/User"


const LoadingScreen = () =>{

  const params = window.location.search.split("?")[1].split("&").map(function(x){
    return x.split("=")
  })

  const json_params = {
    "user_id": params[0][1],
    "secret_key": params[1][1]
  }

  const [redirect, setRedirect] = useState(false)

  accountActivate(json_params).then(res =>{
    localStorage.setItem("token", res.data.jwt)
  }).then(_ => {
    setTimeout(fetchCurrentUserInfo().then(res1 => {
      if(!!res1.data){
        setRedirect(true)
      }
    }),1500)
  }).catch(error => {
    window.location = "/error"
  })

  useEffect(()=>{
    if(redirect === true){
      window.location = "/"
    }
  })

  return (<>
    <div className = "Loading-box">
      <CircularProgress className = "Loading-element"/>
    </div>
  </>)

}

export default LoadingScreen