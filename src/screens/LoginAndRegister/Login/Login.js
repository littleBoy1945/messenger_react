import React from "react"
import "./styles.css"
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import MessengerIcon from "../../../images/messenger-icon.png"
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import { loginUser } from "../../../api/Login"
import useForm from "../../../helpers/forms/useFormLoginAndRegister"
import validate from "../../../validations/login_validation"


const Copyright = () => {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Messenger
        </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}


const Login = () => {

  const { values, errors, handleChange, handleSubmit } = useForm(loginUser, validate)

  return (
    <>
      <div className="Login-root">
        <div className="Login-header">
          <header className="Login-header-container">
            <h1>
              Chào mừng bạn đến với Messenger
            </h1>
          </header>
        </div>
        <div className="Login-form">
          <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className="Login-paper">
              <img src={MessengerIcon} alt="login icon" className="Login-avatar" />
              <h1 className="Login-title">Đăng nhập</h1>
              <ValidatorForm className="Login-form">
                <TextValidator
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  id="email"
                  label="Email hoặc số điện thoại"
                  name="email"
                  autoComplete="email"
                  value={values.email || ""}
                  onChange={handleChange}
                  error={errors.email?true:false}
                  helperText= {errors.email}
                />
                <TextValidator
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  required
                  label="Mật khẩu"
                  name="password"
                  type="password"
                  value={values.password || ""}
                  onChange={handleChange}
                  error={errors.password?true:false}
                  helperText= {errors.password}
                />
                <FormControlLabel
                  control={<Checkbox value="remember" color="primary" />}
                  label="Duy trì đăng nhập"
                />
                <Button
                  fullWidth
                  variant="contained"
                  color="primary"
                  className="Login-submit"
                  onClick = {handleSubmit}
                >
                  Đăng nhập
                </Button>
                <Grid container className="Login-forgotpwd">
                  <Grid item xs>
                    <Link href="/password" variant="body2">
                      Quên mật khẩu ?
                    </Link>
                  </Grid>
                  <Grid item>
                    <Link href="/register" variant="body2">
                      {"Chưa có tài khoản? Đăng kí ngay"}
                    </Link>
                  </Grid>
                </Grid>
              </ValidatorForm>
            </div>

          </Container>

        </div>
      </div>
      <footer className="Login-footer">
        <Box mt={8}>
          <Copyright />
        </Box>
      </footer>
    </>
  );

}
export default Login