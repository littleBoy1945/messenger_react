import React from "react"
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import MessengerIcon from "../../../images/messenger-icon.png"
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import FormControl from '@material-ui/core/FormControl';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import { registerUser } from "../../../api/Register"
import validate from "../../../validations/register_validation"
import useForm from "../../../helpers/forms/useFormLoginAndRegister"

import "./styles.css"

const Copyright = () => {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Messenger
        </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const Register = () => {

  const { values, errors, handleChange, handleSubmit } = useForm(registerUser, validate)

  const submitRegisterForm = () => {
    let submitted = handleSubmit()
    if(submitted === true){
      window.location = "/loading-register"
    }
  }

  return (
    <>
      <div className="Register-root">
        <div className="Register-header">
          <header className="Register-header-container">
            <h1>
              Chào mừng bạn đến với Messenger
              </h1>
          </header>
        </div>
        <div className="Register-form">
          <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className="Register-paper">
              <img src={MessengerIcon} alt="login icon" className="Register-avatar" />
              <h1 className="Register-title">Đăng kí tài khoản</h1>
              <form className="Register-form">
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      autoComplete="fname"
                      name="firstName"
                      variant="outlined"
                      required
                      fullWidth
                      id="firstName"
                      label="Họ"
                      autoFocus
                      value={values.firstName || ""}
                      onChange={handleChange}
                      error={errors.firstName ? true : false}
                      helperText={errors.firstName}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      variant="outlined"
                      required
                      fullWidth
                      id="lastName"
                      label="Tên"
                      name="lastName"
                      autoComplete="lname"
                      value={values.lastName || ""}
                      onChange={handleChange}
                      error={errors.lastName ? true : false}
                      helperText={errors.lastName}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      variant="outlined"
                      required
                      fullWidth
                      id="email"
                      label="Email hoặc số điện thoại"
                      name="email"
                      value={values.email || ""}
                      onChange={handleChange}
                      error={errors.email ? true : false}
                      helperText={errors.email}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <TextField
                      id="date"
                      label="Ngày sinh"
                      type="date"
                      name="birth"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      value={values.birth || ""}
                      onChange={handleChange}
                      error={errors.birth ? true : false}
                      helperText={errors.birth}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <FormControl component="fieldset">
                      <FormLabel component="legend">Giới tính</FormLabel>
                      <RadioGroup
                        aria-label="Gender"
                        name="sex"
                        value={values.sex || ""}
                        onChange={handleChange}
                      >
                        <FormControlLabel value="male" control={<Radio id="sex1" />} label="Nam" />
                        <FormControlLabel value="female" control={<Radio id="sex2" />} label="Nữ" />
                        <FormControlLabel value="other" control={<Radio id="sex3" />} label="Khác" />

                      </RadioGroup>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      variant="outlined"
                      required
                      fullWidth
                      name="password"
                      label="Mật khẩu"
                      type="password"
                      id="password"
                      value={values.password || ""}
                      onChange={handleChange}
                      error={errors.password ? true : false}
                      helperText={errors.password}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      variant="outlined"
                      required
                      fullWidth
                      name="rePassword"
                      label="Gõ lại mật khẩu"
                      type="password"
                      id="rePassword"
                      value={values.rePassword || ""}
                      onChange={handleChange}
                      error={errors.rePassword ? true : false}
                      helperText={errors.rePassword}
                    />
                  </Grid>
                </Grid>
                <div className="Register-submit">
                  <Button
                    fullWidth
                    variant="contained"
                    color="primary"
                    onClick={submitRegisterForm}
                  >
                    Đăng kí
                    </Button>
                </div>
                <Grid container justify="flex-end" className="Register-hasaccount">
                  <Grid item>
                    <Link href="/login" variant="body2">
                      Đã có tài khoản? Đăng nhập
                      </Link>
                  </Grid>
                </Grid>
              </form>
            </div>

          </Container>
        </div>
      </div>
      <footer className="Register-footer">
        <Box mt={5}>
          <Copyright />
        </Box>
      </footer>
    </>
  );
}

export default Register