import React from "react"
import "./styles.css"
import MessengerIcon from "../../images/messenger-icon.png"
import { Link } from "react-router-dom"
import MessengerRightImageContent from "../../images/messenger-right-login-page.png"
import FooterFacebook from "../../images/footer-from-facebook.jpg"
import { Button } from "@material-ui/core"
import MenuIcon from '@material-ui/icons/Menu';

const toggleResponsive = () => {
    var elementLst = document.getElementById("LAR-header-ul1")
    if (elementLst.className === "LAR-header-right-ul1") {
        elementLst.className += "responsive"
    } else {
        elementLst.className = "LAR-header-right-ul1"
    }
}


const LoginAndRegister = () => {

    return (
        <>
            <div className="LAR-root">
                <div className="LAR-headers">
                    <header className="LAR-header">
                        <div className="LAR-sub-header">
                            <div className="LAR-header-left">
                                <Link to="/"><img src={MessengerIcon} alt="icon" className="LAR-icon-header" /></Link>
                            </div>
                            <div onClick={toggleResponsive} className = "LAR-icon">
                                <MenuIcon />
                            </div>
                            <div className="LAR-header-right">
                                <ul className="LAR-header-right-ul1" id="LAR-header-ul1">
                                    <li className="LAR-header-right-li1">
                                        <Link to="/" className="LAR-header-right-li1-link">Phòng họp mặt</Link>
                                    </li>
                                    <li className="LAR-header-right-li1">
                                        <Link to="/" className="LAR-header-right-li1-link">Tính năng</Link>
                                    </li>
                                    <li className="LAR-header-right-li1">
                                        <Link to="/" className="LAR-header-right-li1-link">Quyền riêng tư </Link>
                                    </li>
                                    <li className="LAR-header-right-li1">
                                        <Link to="/" className="LAR-header-right-li1-link">For developers</Link>
                                    </li>

                                </ul>

                            </div>
                        </div>
                    </header>
                </div>
                <div className="LAR-content">
                    <div className="LAR-content-2">
                        <div className="LAR-content-left">
                            <div className="LAR-content-left-slogan">
                                <h1 className="LAR-content-left-slogan-h1">
                                    Tụ tập <br></br> mọi lúc, mọi nơi
                                </h1>
                            </div>
                            <div className="LAR-content-left-title">
                                <p>
                                    Với Messenger, việc kết nối với những người mình yêu mến thật đơn giản và thú vị.
                                </p>
                            </div>

                        </div>
                        <div className="LAR-content-right">
                            <img src={MessengerRightImageContent} alt="right content" />
                        </div>
                        <div className="LAR-signinup-button">
                            <div className="LAR-signin-button">
                                <Link to="/login">
                                    <Button variant="contained" color="primary">
                                        Đăng nhập
                                    </Button>
                                </Link>
                            </div>
                            <div className="LAR-signup-button">
                                <Link to="/register">
                                    <Button variant="contained" color="primary">
                                        Đăng kí
                                    </Button>
                                </Link>
                            </div>
                        </div>
                        <div className="LAR-forgot-pwd">
                            <Link to="/password">Quên mật khẩu</Link>
                        </div>
                    </div>
                </div>
                <footer className="LAR-footer">
                    <div className="LAR-footer-elements">
                        <span className="LAR-footer-element-2"> © Facebook 2021. Logo của Apple và Google Play là nhãn hiệu hàng hóa thuộc chủ sở hữu tương ứng. </span>
                        <span>
                            <img src={FooterFacebook} alt="footer-element" />
                        </span>
                    </div>
                </footer>
            </div>
        </>
    );
}

export default LoginAndRegister