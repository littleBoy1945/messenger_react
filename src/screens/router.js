import LoginAndRegister from "./LoginAndRegister/loginAndRegister"
import React from "react"
import { Route, Switch } from "react-router-dom"
import Login from "./LoginAndRegister/Login/Login"
import Register from "./LoginAndRegister/Register/Register"
import LoadingScreen from "./AccountActivation/AccountActivation"
import HomeScreen from "../screens/HomeScreen/HomeScreen"
import { Redirect } from "react-router-dom";
import ErrorPage from "../screens/404/index"
import Loading from "../screens/loading/loading"

const NotFoundRedirect = () => <Redirect to='/' />

const MessengerRouter = () => {
  return (
    <Switch>
      {
        !!localStorage.getItem("token")=== false?
        <>
          <Route exact path="/" component = {LoginAndRegister} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/loading-register" component = {Loading}/>
          <Route exact path="/account-activation" component={LoadingScreen} />
          <Route exact path="/error" component={ErrorPage}/>
        </>
        :
        <>
          <Route exact path="/" component = {HomeScreen} />
          <Route component = {NotFoundRedirect} />
        </>
      }
      
    </Switch>
  );
}

export default MessengerRouter