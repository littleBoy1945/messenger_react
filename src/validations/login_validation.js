export default function validate(values) {
  let errors = {};
  if (!values.email) {
    errors.email = "Yêu cầu email !"
  } else if (!/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(values.email)) {
    errors.email = "Email không hợp lệ !"
  }

  if (!values.password) {
    errors.password = "Yêu cầu mật khẩu !"
  }else if(values.password.length < 6 || values.password.length > 12){
    errors.password = "Mật khẩu phải từ 6 đến 12 kí tự"
  }

  return errors;
}