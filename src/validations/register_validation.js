export default function validate(values) {
  let errors = {};

  if (!values.firstName) {
    errors.firstName = "Yêu cầu họ !"
  } else if (!/^[a-zA-Z]*$/.test(values.firstName)) {
    errors.firstName = "Họ không hợp lệ !"
  } else if (values.firstName.length > 10) {
    errors.firstName = "Họ phải ít hơn 10 kí tự !"
  }

  if (!values.lastName) {
    errors.lastName = "Yêu cầu tên !"
  } else if (!/^[a-zA-Z]*$/.test(values.lastName)) {
    errors.lastName = "Tên không hợp lệ !"
  } else if (values.lastName.length > 10) {
    errors.lastName = "Tên phải ít hơn 10 kí tự !"
  }

  if (!values.email) {
    errors.email = "Yêu cầu email !"
  } else if (!/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(values.email)) {
    errors.email = "Email không hợp lệ !"
  }

  if (!values.birth) {
    errors.birth = "Ngày sinh không được để trống !"
  }

  if (!values.password) {
    errors.password = "Yêu cầu mật khẩu !"
  }else if(values.password.length < 6 || values.password.length > 12){
    errors.password = "Mật khẩu phải từ 6 đến 12 kí tự"
  }

  if(!values.sex){
    errors.sex = "Giới tính không được để trống !"
  }

  if(values.password !== values.rePassword){
    errors.rePassword = "Không khớp"
  }

  return errors

}

